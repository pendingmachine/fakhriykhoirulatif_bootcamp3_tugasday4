import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import React from "react";

function Users(props) {
  const { users, onUserEdit } = props;

  return users.length ? (
    <Stack
      spacing="4px"
      sx={{
        padding: "4px",
        backgroundColor: "black",
      }}
    >
      {users.map((user, index) => (
        <div className="user" key={index}>
          <div className="left">
            <div className="name">{user.name}</div>
            <div className="address">{user.address}</div>
          </div>
          <div className="right">
            <div className="hobby">{user.hobby}</div>
            <Button variant="contained" onClick={onUserEdit.bind(this, index)}>
              Edit
            </Button>
          </div>
        </div>
      ))}
    </Stack>
  ) : (
    <div className="no-user">
      <div className="zero">0</div>
      <div className="text">User</div>
    </div>
  );
}

export default Users;
