import Alert from "@mui/material/Alert";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";

function UserFormDialog(props) {
  const { open, title, data, onChange, onClose, onSave } = props;

  const [dataIsValid, setDataIsValid] = useState(true);

  const isDataValid = (data) => {
    return data.name && data.address && data.hobby;
  };

  const updateData = (key, value) => {
    const newData = { ...data };
    newData[key] = value;
    onChange(newData);

    // Remove the alert after the user entered a valid data
    if (!dataIsValid && isDataValid(newData)) {
      setDataIsValid(true);
    }
  };

  const handleSave = () => {
    // Prevent user from saving invalid data
    if (!isDataValid(data)) {
      setDataIsValid(false);
      return;
    }

    onSave();
  };

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle
        sx={{
          textAlign: "center",
        }}
      >
        {title}
      </DialogTitle>
      <DialogContent>
        <Stack
          component="form"
          noValidate
          autoComplete="off"
          spacing={2}
          sx={{
            padding: "10px 0",
            width: "300px",
          }}
        >
          <TextField
            required
            label="Name"
            value={data.name}
            onChange={(e) => updateData("name", e.target.value)}
          />
          <TextField
            required
            label="Address"
            value={data.address}
            onChange={(e) => updateData("address", e.target.value)}
          />
          <TextField
            required
            label="Hobby"
            value={data.hobby}
            onChange={(e) => updateData("hobby", e.target.value)}
          />
          {dataIsValid ? null : (
            <Alert severity="error">All fields must be filled</Alert>
          )}
        </Stack>
      </DialogContent>
      <DialogActions
        sx={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Button onClick={handleSave} variant="contained">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default UserFormDialog;
